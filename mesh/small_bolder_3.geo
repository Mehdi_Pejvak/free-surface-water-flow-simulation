
cl1 = 6;
cl2 = .03;
cl3 = 10;

scale = 1.0e-2;         // convert to cm
bolder_width = 6 * scale;
bolder_height = 6 * scale;
out_rad = bolder_width * 5;
extr_bolder = 6 * scale;                         // length of domain prependicular to surface (Z direction)
extr = 30 * scale;                                     // length of domain prependicular to surface (Z direction)
lus = bolder_width * 40;        // length of upstream
lds = bolder_width * 30;        // length of downstream
w = 40 * scale;                                         // Width of domain

// Exterior (bounding box) of mesh
Point(1) = {-lus, -w, 0, cl1};
//+
Point(2) = { lds, -w, 0, cl3};
//+
Point(3) = { lds,  w, 0, cl3};
//+
Point(4) = {-lus,  w, 0, cl1};
//+

// Circle & surrounding structured-quad region
//+
Point(5) = {0,   0, 0, cl2};
//+
Point(6) = {bolder_width/ 2,  bolder_width/ 2, 0, cl2};
//+
Point(7) = {bolder_width/ 2, -bolder_width / 2, 0, cl2};
//+
Point(8) = {-bolder_width/ 2,  bolder_width/ 2, 0, cl2};
//+
Point(9) = {-bolder_width / 2, -bolder_width/ 2, 0, cl2};
//+
Point(12) = {out_rad/Sqrt(2), out_rad/Sqrt(2), 0, 1.0};
//+
Point(13) = {out_rad/Sqrt(2), -out_rad/Sqrt(2), 0, 1.0};
//+
Point(14) = {-out_rad/Sqrt(2), -out_rad/Sqrt(2), 0, 1.0};
//+
Point(15) = {-out_rad/Sqrt(2), out_rad/Sqrt(2), 0, 1.0};
//+
Point(17) = {lds,  out_rad/Sqrt(2), 0, cl2};
//+
Point(18) = {lds, -out_rad/Sqrt(2), 0, cl2};
//+
Point(19) = {-lus,  out_rad/Sqrt(2), 0, cl2};
//+
Point(20) = {-lus, -out_rad/Sqrt(2), 0, cl2};
//+
Point(21) = {-out_rad/Sqrt(2), w, 0, 1.0};
//+
Point(22) = {out_rad/Sqrt(2), w, 0, 1.0};
//+
Point(23) = {-out_rad/Sqrt(2), -w, 0, 1.0};
//+
Point(24) = {out_rad/Sqrt(2), -w, 0, 1.0};
//+
//Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
//Line(3) = {3, 4};
//+
//Line(4) = {4, 1};
//+
Line(5) = {7, 6};
//+
Line(6) = {6, 8};
//+
Line(7) = {13, 14};
//+
Line(8) = {15, 14};
//+
Line(12) = {8, 9};
//+
Line(13) = {9, 7};
//+
Line(14) = {13, 12};
//+
Line(15) = {15, 12};
//+
Line(9)  = {8, 15};
//+
Line(10) = {6, 12};
//+
Line(11) = {7, 13};
//+
Line(16) = {9, 14};
//+
Line(17) = {22, 12};
//+
Line(18) = {21, 15};
//+
Line(19) = {19, 15};
//+
Line(20) = {20, 14};
//+
Line(21) = {23, 14};
//+
Line(22) = {24, 13};
//+
Line(23) = {13, 18};
//+
Line(24) = {12, 17};
//+
Line(25) = {4, 21};
//+
Line(26) = {21, 22};
//+
Line(27) = {22, 3};
//+
Line(28) = {3, 17};
//+
Line(29) = {17, 18};
//+
Line(30) = {18, 2};
//+
Line(31) = {2, 24};
//+
Line(32) = {24, 23};
//+
Line(33) = {23, 1};
//+
Line(34) = {1, 20};
//+
Line(35) = {20, 19};
//+
Line(36) = {19, 4};
//+

Line Loop(1) = {18, -19, 36, 25};
//+
Surface(1) = {1};
//+
Line Loop(2) = {35, 19, 8, -20};
//+
Surface(2) = {2};
//+
Line Loop(3) = {34, 20, -21, 33};
//+
Surface(3) = {3};
//+
Line Loop(4) = {21, -7, -22, 32};
//+
Surface(4) = {4};
//+
Line Loop(5) = {22, 23, 30, 31};
//+
Surface(5) = {5};
//+
Line Loop(6) = {23, -29, -24, -14};
//+
Surface(6) = {6};
//+
Line Loop(7) = {24, -28, -27, 17};
//+
Surface(7) = {7};
//+
Line Loop(8) = {26, 17, -15, -18};
//+
Surface(8) = {8};
//+
Line Loop(9) = {15, -10, 6, 9};
//+
Surface(9) = {9};
//+
Line Loop(10) = {9, 8, -16, -12};
//+
Surface(10) = {10};
//+
Line Loop(11) = {16, -7, -11, -13};
//+
Surface(11) = {11};
//+
Line Loop(12) = {10, -14, -11, 5};
//+
Surface(12) = {12};

layer_botom = 12;

Extrude {0, 0, extr_bolder} { Surface{1,2,3,4,5,6,7,8,9,10,11,12,13}; Layers{layer_botom}; Recombine;}
//+
Curve Loop(13) = {261, 239, -216, -283};
//+
Surface(301) = {13};
//+

Extrude {0, 0, extr-extr_bolder} { Surface{190,168,146,300,278,256,234,124,212,102,80,58,301};Layers{{5,8,20},{0.1,0.3,1.0}}; Recombine;}

cell_num_ud = 40;   // num of cell vertical lines up/down of cylinder
cell_num_diag = 50; // num of cell over diagonal lines insde outer cylinder
cell_num_cyl = 40;  // num of cell around cylinder
cell_num_us = 200;
cell_num_ds = 120;

grad1 = 1.02;   // grading in diagonal lines
grad2 = 1.01;   // grading downstream horizonal lines
grad3 = 0.7;    // grading in upstream horizonla lines
grad4 = 0.7;   // grading up/down vertical lines

Transfinite Line {36,-18,-17,-28,-34,-21,-22,30} = cell_num_ud Using Bump grad4;  // up/down vertical lines
Transfinite Line {-25,-19,-20,33,85,63,39,41}= cell_num_us Using Bump grad3; // upstream horizonla lines
Transfinite Line {24,27,23,-31} = cell_num_ds Using Progression grad2; // upstream horizontal lines
Transfinite Line {26,35,32,29} = cell_num_cyl; // end lines in cross in middle 
Transfinite Line {5,6,12,13,7,8,14,15} = cell_num_cyl;  // circles
Transfinite Line {16,11,9,10} = cell_num_diag Using Progression grad1; // diagonals

Transfinite Surface{1,2,3,4,5,6,7,8,9,10,11,12,13,190,168,146,300,278,256,234,124,212,102,80,58,301};
Recombine Surface {1,2,3,4,5,6,7,8,9,10,11,12,13,190,168,146,300,278,256,234,124,212,102,80,58,301};

Physical Surface("inlet") = {53,67,89,560,530,508};
Physical Surface("outlet") = {181,159,141,362,336,314};
Physical Surface("bolder") = {255,229,299,277,301};
Physical Surface("wall") ={57,101,123,199,145,185,564,520,486,476,318,366};
Physical Surface("bottom") ={1,2,3,4,5,6,7,8,9,10,11,12};
Physical Surface("atmosphere") ={521,543,565,477,411,433,455,389,499,587,367,345,323};
Physical Surface("symmetric") = {79,49,45,97,75,207,167,115,251,233,225,273,189,119,137,163,102,80,58,124,212,234,256,278,300,146,168,190,534,512,498,424,402,344,442,384,398,420,376,410,432,450,388,464,354,322,332,310};
Physical Volume("internalField") ={1,2,3,4,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};




